#include "morsecharimpl.h"

MorseCharImpl::MorseCharImpl(std::string code)
    : m_code(code)
{
}

std::string MorseCharImpl::toString() const
{
    return m_code;
}
