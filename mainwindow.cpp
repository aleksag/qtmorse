#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_textEdit_textChanged()
{
    auto txt = ui->textEdit->toPlainText().toStdString();
    try {
        auto translated = m_translator.translate(txt);
        ui->textEdit_2->setText(QString::fromStdString(translated));

    } catch (std::runtime_error& e)
    {
        ui->statusbar->showMessage(QString::fromStdString(e.what()), 2000);
    }
}
