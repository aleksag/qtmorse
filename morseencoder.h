#ifndef MORSEENCODER_H
#define MORSEENCODER_H
#include <morsechar.h>
#include <memory>

class MorseEncoder
{
public:
    MorseEncoder() = default;
    std::unique_ptr<MorseChar> encode(char) const;
};

#endif // MORSEENCODER_H
