#include "morsetranslator.h"


std::string MorseTranslator::translate(std::string inputStr) const
{
    std::string result;
    for (auto& token : m_inputStrParser.parse(inputStr)) {
        if (std::holds_alternative<char>(token))
        {
            result.append(m_encoder.encode(std::get<char>(token))->toString());
            result.push_back(' ');
        }
        else
        {
            auto& mCodeToken = std::get<std::unique_ptr<MorseChar>>(token);
            result.push_back(m_decoder.decode(*mCodeToken));
        }

    }
    return result;
}
