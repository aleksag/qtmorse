#ifndef MORSECHARIMPL_H
#define MORSECHARIMPL_H

#include "morsechar.h"

class MorseCharImpl : public MorseChar
{
public:
    MorseCharImpl(std::string code);
    std::string toString() const override;
private:
    std::string m_code;
};

#endif // MORSECHARIMPL_H
