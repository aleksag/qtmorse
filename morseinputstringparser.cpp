#include "morseinputstringparser.h"
#include "morsecharimpl.h"

std::vector<std::variant<char, std::unique_ptr<MorseChar>>> MorseInputStringParser::parse(const std::string& input) const
{
    std::vector<std::variant<char, std::unique_ptr<MorseChar>>> result;

    std::string morseString;
    for (auto& c : input)
    {
        switch(c) {
        case '-':
        case '.':
            morseString.push_back(c);
            break;
        default:
            if (!morseString.empty()) {
                result.push_back(std::make_unique<MorseCharImpl>(morseString));
                morseString.clear();
            }

            if ((c >= 'A' && c <= 'Z') ||
                (c >= 'a' && c <= 'z'))
            {
                // only english letters, ignore other stuff
                result.push_back(c);
            }
        }
    }

    if (!morseString.empty())
        result.push_back(std::make_unique<MorseCharImpl>(morseString));

    return result;
}
