#ifndef MORSECHAR_H
#define MORSECHAR_H

#include <string>

class MorseChar
{
public:
    virtual std::string toString() const = 0;
};

#endif // MORSECHAR_H
