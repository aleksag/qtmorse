#ifndef MORSEDECODER_H
#define MORSEDECODER_H

#include <morsechar.h>

class MorseDecoder
{
public:
    MorseDecoder() = default;
    char decode(const MorseChar&) const;
};

#endif // MORSEDECODER_H
