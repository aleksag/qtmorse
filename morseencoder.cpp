#include "morseencoder.h"
#include "morsecharimpl.h"

#include <map>

namespace
{
const std::map<char, std::string> CHAR_TO_MORSE {
    {'A',".-"},
    {'B',"-..."},
    {'C',"-.-."},
    {'D',"-.."},
    {'E',"."},
    {'F',"..-."},
    {'G',"--."},
    {'H',"...."},
    {'I',".."},
    {'J',".---"},
    {'K',"-.-"},
    {'L',".-.."},
    {'M',"--"},
    {'N',"-."},
    {'O',"---"},
    {'P',".--."},
    {'Q',"--.-"},
    {'R',".-."},
    {'S',"..."},
    {'T',"-"},
    {'U',"..-"},
    {'V',"...-"},
    {'W',".--"},
    {'X',"-..-"},
    {'Y',"-.--"},
    {'Z',"--.."}
};
}

std::unique_ptr<MorseChar> MorseEncoder::encode(char c) const
{
    auto&& result = CHAR_TO_MORSE.find(std::toupper(c));
    if (result == CHAR_TO_MORSE.end())
        throw std::runtime_error("could not encode character");

    return std::make_unique<MorseCharImpl>(result->second);
}
