#ifndef MORSETRANSLATOR_H
#define MORSETRANSLATOR_H

#include <string>
#include "morsedecoder.h"
#include "morseencoder.h"
#include "morseinputstringparser.h"

class MorseTranslator
{
public:
    MorseTranslator() = default;
    std::string translate(std::string inputStr) const;
private:
    MorseEncoder m_encoder;
    MorseDecoder m_decoder;
    MorseInputStringParser m_inputStrParser;
};

#endif // MORSETRANSLATOR_H
