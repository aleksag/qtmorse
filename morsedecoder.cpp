#include "morsedecoder.h"
#include <map>
#include <string>

namespace
{
const std::map<std::string, char> MORSE_TO_CHAR = {
    {".-", 'A'},
    {"-...", 'B'},
    {"-.-.", 'C'},
    {"-..", 'D'},
    {".", 'E'},
    {"..-.", 'F'},
    {"--.", 'G'},
    {"....", 'H'},
    {"..", 'I'},
    {".---", 'J'},
    {"-.-", 'K'},
    {".-..", 'L'},
    {"--", 'M'},
    {"-.", 'N'},
    {"---", 'O'},
    {".--.", 'P'},
    {"--.-", 'Q'},
    {".-.", 'R'},
    {"...", 'S'},
    {"-", 'T'},
    {"..-", 'U'},
    {"...-", 'V'},
    {".--", 'W'},
    {"-..-", 'X'},
    {"-.--", 'Y'},
    {"--..", 'Z'}
};
}

char MorseDecoder::decode(const MorseChar& morseChar) const
{
    auto&& result = MORSE_TO_CHAR.find(morseChar.toString());
    if (result == MORSE_TO_CHAR.end())
        throw std::runtime_error("Could not decode " + morseChar.toString());

    return result->second;

}


