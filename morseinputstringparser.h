#ifndef MORSEINPUTSTRINGPARSER_H
#define MORSEINPUTSTRINGPARSER_H

#include <variant>
#include <vector>
#include <memory>

#include "morsechar.h"

class MorseInputStringParser
{
public:
    MorseInputStringParser() = default;
    std::vector<std::variant<char, std::unique_ptr<MorseChar>>> parse(const std::string& input) const;

};

#endif // MORSEINPUTSTRINGPARSER_H
